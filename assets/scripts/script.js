
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

const horse1 = () => {
	let elem = document.getElementById("myBar");
	let width = 1;
	let id = setInterval(frame, getRandomInt(50, 80));
	function frame() {
		if (width >= 100) {
			clearInterval(id);
		} else {
			width++;
			elem.style.width = width + '%';
		}
	}
}

const horse2 = () => {
	let elem = document.getElementById("myBar1");
	let width = 1;
	let id = setInterval(frame, getRandomInt(50, 80));
	function frame() {
		if (width >= 100) {
			clearInterval(id);
		} else {
			width++;
			elem.style.width = width + '%';
		}
	}
}

const horse3 = () => {
	let elem = document.getElementById("myBar2");
	let width = 1;
	let id = setInterval(frame, getRandomInt(50, 80));
	function frame() {
		if (width >= 100) {
			clearInterval(id);
		} else {
			width++;
			elem.style.width = width + '%';
		}
	}
}

const horse4 = () => {
	let elem = document.getElementById("myBar3");
	let width = 1;
	let id = setInterval(frame, getRandomInt(50, 80));
	function frame() {
		if (width >= 100) {
			clearInterval(id);
		} else {
			width++;
			elem.style.width = width + '%';
		}
	}
}



const allButtons = () => {
	horse1();
	horse2();
	horse3();
	horse4();
}

const reset = () => {
	location.reload();
}